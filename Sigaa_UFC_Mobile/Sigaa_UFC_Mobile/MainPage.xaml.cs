﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using System.Net.Http;

namespace Sigaa_UFC_Mobile
{
    public partial class MainPage : ContentPage
    {
        private static readonly HttpClient client = new HttpClient();
        private String _response;
        public MainPage()
        {
            InitializeComponent();

            acessarSigaa();
        }

        public void acessarSigaa()
        {
            var htmlSource = new HtmlWebViewSource();
            htmlSource.Html = DependencyService.Get<ILoadHomePage>().Get();
            webviu.Source = htmlSource;

           
            ToolbarItems.Add(new ToolbarItem("Voltar", "voltar.png", () => { webviu.GoBack(); }));
        }

       
    }
}
