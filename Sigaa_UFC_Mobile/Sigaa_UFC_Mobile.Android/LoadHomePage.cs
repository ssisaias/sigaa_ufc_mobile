﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Xamarin.Forms;
using Sigaa_UFC_Mobile.Droid;
using System.IO;

[assembly : Dependency (typeof(LoadHomePage))]
namespace Sigaa_UFC_Mobile.Droid
{
    class LoadHomePage : ILoadHomePage
    {
        public string Get()
        {
            String html = "";
            var assetManager = Xamarin.Forms.Forms.Context.Assets;
            using (var streamReader = new StreamReader(assetManager.Open("home.html")))
            {
                html = streamReader.ReadToEnd();
            }
            return html;
        }
    }
}